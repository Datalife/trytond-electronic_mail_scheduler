# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import electronic_mail
from . import template
from . import ir


def register():
    Pool.register(
        electronic_mail.Mailbox,
        electronic_mail.ElectronicMail,
        template.Template,
        ir.Cron,
        module='electronic_mail_scheduler', type_='model')
